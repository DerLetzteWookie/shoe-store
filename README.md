# React Webshop prototyp

## Features

Folgende Features beinhaltet der Shop bis jetzt: 

- Datenspeicherung im LocalStorage
- Login / Registration / als Gast bestellen
- Produkte ansehen
- Produktdetails
- Profil
    - Profil ansehen
    - Bestellungen ansehen
- Cart
- Checkout

Auf anfrage bin ich bereit gerne eine Datenspeicherung mittels Datenbank anzulegen und ein Backend mittels PHP 8 aufzusetzen.

## Anmerkungen

### Login

Ich habe bewusst Zugangsdaten im Login-Bereich vorausgefüllt gelassen, um das Testen zu erleichtern.

### LESS

Alle CSS-Klassen die ich benutzte sind mit einem `fkf-` Präfix versehen. Der Präfix kann von Projekt zu Projekt unterschiedlich heißen. Im normalfall sind es Anfangskürzel vom Projekt oder Kunden.
Ich benutze diese Präfixe in meinen Projekten um zu verhindern, das andere Frameworks meine CSS anweisungen überschreiben. Ebenfalls dient es der Lesbarkeit im Code. Durch die Präfixe ist es einfach herauszufiltern welche Klassen von Hand- und welche Klassen vom Framework kommen.
