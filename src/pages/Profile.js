import React, {useContext, useEffect, useState} from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import API from "../classes/API";
import { AppStateContext } from '../components/AppStateProvider';
import Loading from "../components/Loading";
import ProductListItem from "../components/ProductListItem";

export default function Profile() {
    const { User, SetUser } = useContext(AppStateContext);

    if( !User ) {
        return <Navigate to={"/login"} />
    }
    return(
        <Routes>
            <Route path="/" element={<ProfileView user={User} SetUser={SetUser} />}>
            </Route>
            <Route path="/edit" element={<ProfileEdit />} />
        </Routes>
    )
}

function ProfileView({user, SetUser}) {
    const [isLoading, setIsLoading] = useState(true);
    const [orders, setOrders] = useState([]);

    var ordersContent = <li>Keine Bestellung gefunden.</li>;

    /***
     * Quelle: https://bobbyhadz.com/blog/javascript-format-date-dd-mm-yyyy
    */
    function padTo2Digits(num) {
        return num.toString().padStart(2, '0');
      }
      
    function formatDate(date) {
        return [
            padTo2Digits(date.getDate()),
            padTo2Digits(date.getMonth() + 1),
            date.getFullYear(),
        ].join('.');
    }
    /**
     * END
     */

    useEffect(function(){
        if( isLoading && user ) {
            API.getUserOrders(user.id).then(response => {
                if( response.status === 200 ) {
                    setOrders( response.data, setIsLoading( false ) );
                }else {
                    setIsLoading( false );
                }
            }).catch(error => {
                console.log(error.message);
            })
        }
    });

    if( isLoading ) {
        return <Loading />;
    }

    if( orders && orders.length > 0 ) {
        ordersContent = orders.map(order => {
            var totalPrice = 0;
            if( order.products ) {
                order.products.forEach(product => {
                    totalPrice += (parseInt(product.amount) * product.price);
                });
            }
            return <li className="fkf-order">
                <dl className="fkf-dl">
                    <dt>Bestellnummer:</dt>
                    <dd>{order.id ? order.id : ""}</dd>
                    <dt>Bestelldatum:</dt>
                    <dd>{order.date ? formatDate( new Date(order.date) ) : "-"}</dd>
                    <dt>Gesamtpreis:</dt>
                    <dd>{totalPrice.toFixed(2)}€</dd>
                    <dt>Versandart</dt>
                    <dd>
                        {order.shippingMethod ? 
                            <img src={`${process.env.PUBLIC_URL}/assets/images/shipping/${order.shippingMethod.toLowerCase()}.jpg`} alt="DHL" title="DHL" />
                        : "-"}
                    </dd>
                    <dt>Bezahlmethode</dt>
                    <dd>
                        {order.paymentMethod ? 
                            <img src={`${process.env.PUBLIC_URL}/assets/images/payment/${order.paymentMethod.toLowerCase()}.jpg`} alt="DHL" title="DHL" />
                        : "-"}
                    </dd>
                    {console.log(order)}
                </dl>
                <p></p>
                <ul className="fkf-order__products">
                    {order.products.map((product, index) => <ProductListItem key={`PROFILE_PRODUCT_ITEM_${index}`} product={product}><p className="fkf-product-item__amount">{product.amount ? product.amount : 0}</p></ProductListItem>)}
                </ul>
            </li>;
        })
    }
    


    return( <div className="fkf-profile fkf-page-wrapper fkf-checkout__wrapper">
        <h2>Deine Persönlichendaten:</h2>
        <dl className="fkf-dl">
            <dt>E-Mail:</dt>
            <dd>{(user && user.mail) ? user.mail : "-"}</dd>
            <dt>Vorname:</dt>
            <dd>{(user && user.firstname) ? user.firstname : "-"}</dd>
            <dt>Nachname:</dt>
            <dd>{user && user.lastname ? user.lastname : "-"}</dd>
            <dt>Straße:</dt>
            <dd>{(user && user.address && user.address.street) ? user.address.street : "-"}</dd>
            <dt>Nummer:</dt>
            <dd>{(user && user.address && user.address.number) ? user.address.number : "-"}</dd>
            <dt>Postleitzahl:</dt>
            <dd>{(user && user.address && user.address.zipcode) ? user.address.zipcode : "-"}</dd>
            <dt>Ort:</dt>
            <dd>{(user && user.address && user.address.city) ? user.address.city : "-"}</dd>
        </dl> 
        <h2>Deine Bestellungen:</h2>
        <ul className="fkf-profile__orders">
            {ordersContent}
        </ul>
        <button onClick={() => SetUser(null)} className="fkf-btn fkf-profile__logout-btn">
            Logout
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>
        </button>
    </div>);
}

function ProfileEdit() {
    return( <div className="fkf-profile">
        <h1>Profile Edit</h1>    
    </div>);
}