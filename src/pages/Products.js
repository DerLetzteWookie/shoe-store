import React from "react";
import { Routes, Route} from "react-router-dom";

import ProductDetail from './../components/ProductDetail';
import ProductList from "../components/ProductList";

export default function Products() {
    return(
        <div>
            <Routes>
                <Route path="/" element={<div className="fkf-page-wrapper"><div className="fkf-teaser"><h1>Schuhe für alle Lebenslagen</h1><img src={`${process.env.PUBLIC_URL}/assets/images/teaser-1.jpg`} alt="Urban Shoe" title="Urban Shoe" /></div><ProductList /></div>} />
                <Route path=":productId" element={<ProductDetail />} />
            </Routes>
        </div>
    )
}

