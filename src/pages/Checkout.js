import React, { useEffect, useContext, useState } from "react";
import { Routes, Route, Navigate, useParams, NavLink } from "react-router-dom";
import { AppStateContext } from '../components/AppStateProvider';

import Loading from '../components/Loading';

import ProductListItem from "../components/ProductListItem";

import API from "../classes/API";

import Order from "../classes/Order";

var md5 = require('md5');

export default function Checkout(){
    const { User, CartIsOpen, SetCartIsOpen } = useContext(AppStateContext);
    const [order, setOrder] = useState( new Order() );
    const routerParams = useParams();

    useEffect(function(){
        if( CartIsOpen ) {
            SetCartIsOpen( false );
        }
    });

    if( User === null && routerParams["*"] !== "login" ) {
        return <Navigate to={"/checkout/login"} />
    }
    
    if( !order.userId && User && routerParams["*"] !== "personal-data") {
        return <Navigate to={"/checkout/personal-data"} />
    }
    
    if( (!order.shippingMethod || !order.paymentMethod) && routerParams["*"] === "overviw" && routerParams["*"] !== "shipping-and-payment-method") {
        return <Navigate to={"/checkout/shipping-and-payment-method"} />
    }

    return( 
        <Routes>
            <Route path="/" element={<h1>Checkout</h1>}>
            </Route>
            <Route path="/login" element={<CheckoutLogin/>} />
            <Route path="/personal-data" element={<CheckoutPersonalData order={order} setOrder={setOrder} />} />
            <Route path="/shipping-and-payment-method" element={<CheckoutShippingPaymentMethod order={order} setOrder={setOrder} />} />
            <Route path="/overview" element={<CheckoutOverview order={order} />} />
            <Route path="/finish" element={<CheckoutFinish />} />
        </Routes>
    )
}

function CheckoutLogin() {
    const [ mail, setMail ] = useState( "" );
    const [ password, setPassword ] = useState( "" );
    const [orderAsGuest, setOrderAsGuest] = useState(false);
    const { SetUser } = useContext(AppStateContext);

    const loginHandler = (e) => {
        e.preventDefault();
        API.login(mail, md5(password) ).then(response => {
            if( response.status === 200 ) {
                SetUser( response.data );
            }
        }).catch(error => {
            console.log( error.message );
        });
    }
    
    const loginAsGuestHandler = (e) => {
        e.preventDefault();
        API.loginAsGuest(mail).then(response => {
            if( response.status === 200 ) {
                SetUser( response.data );
            }
        }).catch(error => {
            console.log( error.message );
        });
    }

    return(
        <div className="fkf-checkout">
            <div className="fkf-page-wrapper fkf-login">
                <div className="wrapper">
                    <h2>Login</h2>
                    <form className="fkf-form" onSubmit={orderAsGuest ? loginAsGuestHandler : loginHandler}>
                        <label className="fkf-input">
                            E-Mail
                            <input type="text" value={mail} onChange={(e) => setMail(e.target.value)} />
                        </label>
                        {orderAsGuest ? "" : 
                        <>
                            <label className="fkf-input">
                                Password
                                <input type="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                            </label>
                        </>
                        }
                        <label className="fkf-checkbox">
                            <input type="checkbox" value={orderAsGuest} onChange={() => setOrderAsGuest(!orderAsGuest)}/>
                            <span className="fkf-checkbox__input">&nbsp;</span>
                            <span>als Gast bestellen</span>
                        </label>
                        <button className="fkf-btn">{orderAsGuest ? "weiter als Gast" : "Login"}</button>
                    </form>
                    </div>
            </div>
            
        </div>
        
    )
}

function CheckoutPersonalData({order,setOrder}) {
    const { User, SetUser } = useContext(AppStateContext);
    const [id] = useState( User.id ? User.id : null );
    const [mail, setMail] = useState( User.mail ? User.mail : "" );
    const [firstname, setFirstname] = useState( User.firstname ? User.firstname : "" );
    const [lastname, setLastname] = useState( User.lastname ? User.lastname : "" );
    const [street, setStreet] = useState( (User.address && User.address.street) ? User.address.street : "" );
    const [number, setNumber] = useState( (User.address && User.address.number) ? User.address.number : "" );
    const [zipcode, setZipcode] = useState( (User.address && User.address.zipcode) ? User.address.zipcode : "" );
    const [city, setCity] = useState( (User.address && User.address.city) ? User.address.city : "" );
    const [isPersonalDataFinish, setIsPersonalDataFinish] = useState( false );

    const personalDataHandler = (e) => {
        e.preventDefault();
        
        API.updateUser({
            id:id,
            mail:mail,
            firstname:firstname,
            lastname:lastname,
            address:{
                street:street,
                number:number,
                zipcode:zipcode,
                city:city
            }
        }).then(response => {
            if( response.status === 200 ) {
                var updatedOrder = order;
                updatedOrder.userId = response.data.id;
                SetUser( response.data, setOrder (updatedOrder, setIsPersonalDataFinish( true ) ) );
            }
        });
    }

    if( isPersonalDataFinish ) {
        return <Navigate to={"/checkout/shipping-and-payment-method"} />
    }

    return(
        <div className="fkf-page-wrapper">
            <div className="fkf-checkout__wrapper">
                <h2>Persönliche Daten</h2>
                <form className="fkf-form" onSubmit={personalDataHandler}>
                    <label className="fkf-input">
                        E-Mail
                        <input type="text" value={mail} onChange={(e) => setMail(e.target.value)} required/>
                    </label>
                    <label className="fkf-input">
                        Vorname
                        <input type="text" value={firstname} onChange={(e) => setFirstname(e.target.value)} required/>
                    </label>
                    <label className="fkf-input">
                        Nachname
                        <input type="text" value={lastname} onChange={(e) => setLastname(e.target.value)} required/>
                    </label>
                    <label className="fkf-input">
                        Straße
                        <input type="text" value={street} onChange={(e) => setStreet(e.target.value)} required/>
                    </label>
                    <label className="fkf-input">
                        Hausnummer
                        <input type="text" value={number} onChange={(e) => setNumber(e.target.value)} required/>
                    </label>
                    <label className="fkf-input">
                        Postleitzahl
                        <input type="text" value={zipcode} onChange={(e) => setZipcode(e.target.value)} required/>
                    </label>
                    <label className="fkf-input">
                        Ort
                        <input type="text" value={city} onChange={(e) => setCity(e.target.value)} required/>
                    </label>
                    <button className="fkf-btn">Weiter</button>
                </form>
            </div>
        </div>
    )
}

function CheckoutShippingPaymentMethod({order, setOrder}) {

    return(
        <div className="fkf-checkout__methods fkf-page-wrapper">
            <div className="fkf-checkout__wrapper">
                <h2>Versandart</h2>
                <p>Wähle eine Verdandart aus.</p>
                <ul>
                    <li>
                        <input type="radio" id="dhl" name="shipping-method" value="Dhl" checked={order.shippingMethod === "Dhl"} onChange={(e) => setOrder({...order,shippingMethod:e.target.value})} />
                        <label htmlFor="dhl">
                            <img src={`${process.env.PUBLIC_URL}/assets/images/shipping/dhl.jpg`} alt="DHL" title="DHL" />
                        </label>
                    </li>
                    <li>
                        <input type="radio" id="hermes" name="shipping-method" value="Hermes" checked={order.shippingMethod === "Hermes"} onChange={(e) => setOrder({...order,shippingMethod:e.target.value})} />
                        <label htmlFor="hermes">
                            <img src={`${process.env.PUBLIC_URL}/assets/images/shipping/hermes.jpg`} alt="Hermes" title="Hermes" />
                        </label>
                    </li>
                    <li>
                        <input type="radio" id="ups" name="shipping-method" value="Ups" checked={order.shippingMethod === "Ups"} onChange={(e) => setOrder({...order,shippingMethod:e.target.value})} />
                        <label htmlFor="ups">
                            <img src={`${process.env.PUBLIC_URL}/assets/images/shipping/ups.jpg`} alt="Ups" title="Ups" />
                        </label>
                    </li>
                </ul>
                <h2>Bezahlmethode</h2>
                <p>Wähle eine Bezahlmethode aus.</p>
                <ul>
                    <li>
                        <input type="radio" id="paypal" name="payment-method" value="Paypal" checked={order.paymentMethod === "Paypal"} onChange={(e) => setOrder({...order,paymentMethod:e.target.value})} />
                        <label htmlFor="paypal">
                            <img src={`${process.env.PUBLIC_URL}/assets/images/payment/paypal.jpg`} alt="Paypal" title="Paypal" />
                        </label>
                    </li>
                    <li>
                        <input type="radio" id="klarna" name="payment-method" value="Klarna" checked={order.paymentMethod === "Klarna"} onChange={(e) => setOrder({...order,paymentMethod:e.target.value})} />
                        <label htmlFor="klarna">
                            <img src={`${process.env.PUBLIC_URL}/assets/images/payment/klarna.jpg`} alt="Klarna" title="Klarna" />
                        </label>
                    </li>
                    <li>
                        <input type="radio" id="sepa" name="payment-method" value="SEPA-Lastschrift" checked={order.paymentMethod === "SEPA-Lastschrift"} onChange={(e) => setOrder({...order,paymentMethod:e.target.value})} />
                        <label htmlFor="sepa">
                            <img src={`${process.env.PUBLIC_URL}/assets/images/payment/sepa-lastschrift.jpg`} alt="SEPA-Lastschrift" title="SEPA-Lastschrift" />
                        </label>
                    </li>
                </ul>
                {(order.paymentMethod && order.shippingMethod) ? <NavLink className="fkf-btn" to="/checkout/overview">Übersicht</NavLink> : ""}
            </div>
        </div>
    )
}

function CheckoutOverview({order}) {
    const [isLoading, setIsLoading] = useState(true);
    const [isOrderFinish, setIsOrderFinish] = useState(false);
    const [cart, setCart] = useState([]);
    const { User } = useContext(AppStateContext);
    var cartValue = 0;

    const finishCheckoutHandler = () => {
        if( !cart && !cart.products && cart.products.length <= 0 ) { return; }
        if( !User || !User.id ) { return; }
        order.products = cart.products;
        order.userId = User.id;
        API.finishOrder(order).then(response => {
            if( response.status === 200 ) {
                setIsOrderFinish( true );
            }
        })
    }

    useEffect(function() {
        if( isLoading ) {
            API.getCart().then(response => {
                if( response.status === 200 ) {
                    setCart( response.data, setIsLoading( false ) );
                } else {
                    setIsLoading( false );
                }
            });
        }
    }, [isLoading]);

    if( cart && cart.products ) {
        cart.products.forEach(product => {
            cartValue += (parseInt(product.amount) * product.price)
        });
    }

    if( isLoading ) {
        return <Loading />;
    }

    if( isOrderFinish ) {
        return <Navigate to={"/checkout/finish"} />
    }
    return(
        <div className="fkf-checkout__overview fkf-page-wrapper">
            <div className="fkf-checkout__wrapper">
                <h2>Übersicht</h2>
                <h3>Persönliche Daten</h3>
                <dl className="fkf-dl">
                    <dt>Vorname</dt>
                    <dd>{User.firstname ? User.firstname : ""}</dd>
                    <dt>Nachname</dt>
                    <dd>{User.lastname ? User.lastname : ""}</dd>
                    <dt>Straße</dt>
                    <dd>{(User.address && User.address.street) ? User.address.street : ""}</dd>
                    <dt>Nummer</dt>
                    <dd>{(User.address && User.address.number) ? User.address.number : ""}</dd>
                    <dt>Postleitzahl</dt>
                    <dd>{(User.address && User.address.zipcode) ? User.address.zipcode : ""}</dd>
                    <dt>Ort</dt>
                    <dd>{(User.address && User.address.city) ? User.address.city : ""}</dd>
                </dl>
                <h3>Zahlungsmethode</h3>
                {order.paymentMethod ? 
                    <img src={`${process.env.PUBLIC_URL}/assets/images/payment/${order.paymentMethod.toLowerCase()}.jpg`} alt="" title="" />
                : ""}
                <h3>Liefermethode</h3>
                {order.shippingMethod ? 
                    <img src={`${process.env.PUBLIC_URL}/assets/images/shipping/${order.shippingMethod.toLowerCase()}.jpg`} alt="" title="" />
                : ""}

                <h3>Produkte</h3>
                <ul>
                    {(cart && cart.products) ? cart.products.map((product,index) => <ProductListItem key={`CHECKOUT_PRODUCT_ITEM_${index}`} product={product}><p className="fkf-product-item__amount">{product.amount ? product.amount : 0}</p></ProductListItem>) : ""}
                </ul>
                <p className="fkf-checkout__overview__total-price">Total: {parseFloat(cartValue).toFixed(2)}€</p>
                <button className="fkf-btn" onClick={finishCheckoutHandler}>jetzt Bestellen</button>
            </div>
            </div>
    )
}

function CheckoutFinish() {
    
    return(
        <div className="fkf-checkout__finish fkf-checkout__wrapper fkf-page-wrapper">
            <h1>Vielen Dank für deine Bestellung!</h1>
            <NavLink to="/" title="Zurück zum Shop" className="fkf-btn small">zurück zum Shop</NavLink>
        </div>
    )
}