import React from "react";
import { NavLink } from "react-router-dom";

import ProductList from "../components/ProductList";

export default function Home() {

    return(
        <div className="fkf-home">
            <div className="fkf-home__header">
                <img src={`${process.env.PUBLIC_URL}/assets/images/header-1.jpg`} alt="" title="" />
                <img src={`${process.env.PUBLIC_URL}/assets/images/header-2.jpg`} alt="" title="" />
                <img src={`${process.env.PUBLIC_URL}/assets/images/header-3.jpg`} alt="" title="" />
                <div>
                    <h1>Wir bieten mehr als nur Schuhe<br/>Wir bieten ein Lebensgefühl</h1>
                    <p></p>
                    <NavLink to="/products" className="fkf-btn white">Zu unseren Produkten</NavLink>
                </div>
            </div>
            <div className="fkf-page-wrapper">
                <ProductList>
                    <h2>Unsere Produkte</h2>
                    <p>Die neusten schuhe für jeden Anlass.</p>
                </ProductList>
            </div>
        </div>
    )
}