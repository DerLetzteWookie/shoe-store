import React, {useContext, useState} from "react";
import { Navigate } from 'react-router-dom';
import API from "../classes/API";
import { AppStateContext } from "../components/AppStateProvider";

import User from "../classes/User";

var md5 = require('md5');

export default function Login({redirectTarget}) {
    const [ mail, setMail ] = useState( "admin@shop.de" );
    const [ password, setPassword ] = useState( "admin" );
    const [ passwordRepeat, setPasswordRepeat ] = useState( "" );
    const [showRegistration, setShowRegistration] = useState( false );
    const [ isLoginSuccessful, setIsLoginSuccessful ] = useState( false );

    const { SetUser } = useContext(AppStateContext);

    const onLoginSubmit = function(e) {
        e.preventDefault();
        API.login(mail,md5(password)).then(response => {
            if( response.status === 200 ) {
                SetUser( new User( response.data ), setIsLoginSuccessful( true,  ) );
            }
        }).catch(error => {
            console.log(error);
        });
    }

    const onRegistrationSubmit = function(e) {
        e.preventDefault();
        if( md5(password) !== md5(passwordRepeat) ) { return; }

        API.registration(mail,md5(password)).then(response => {
            if( response.status === 200 ) {
                SetUser( new User( response.data ), setIsLoginSuccessful( true,  ) );
            }
        }).catch(error => {
            console.log(error);
        });
    }

    if( isLoginSuccessful ) {
        if( redirectTarget ) {
            return <Navigate replace to={redirectTarget} />;
        }else {
            return <Navigate replace to="/profile" />;
        }
    }

    if( showRegistration ) {
        return(
            <div className="fkf-login">
                <div className="wrapper">
                    <h1>Registrieren</h1>
                    <form className="fkf-form" onSubmit={onRegistrationSubmit}>
                        <label className="fkf-input">
                            <span>E-Mail</span>
                            <input type="text" value={mail} onChange={(e) => setMail(e.target.value)} />
                        </label>
                        <label className="fkf-input">
                            <span>Password</span>
                            <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                        </label>
                        <label className="fkf-input">
                            <span>Password wiederholen</span>
                            <input type="password" value={passwordRepeat} onChange={(e) => setPasswordRepeat(e.target.value)} />
                        </label>
                        <button className="fkf-btn">Registrieren</button>
                    </form>
                </div>
            </div>    
        )
    }

    return(
        <div className="fkf-login">
            <div className="wrapper">
                <h1>Login</h1>
                <form className="fkf-form" onSubmit={onLoginSubmit}>
                    <label className="fkf-input">
                        <span>E-Mail</span>
                        <input type="text" value={mail} onChange={(e) => setMail(e.target.value)} />
                    </label>
                    <label className="fkf-input">
                        <span>Password</span>
                        <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                    </label>
                    <button type="button" className="fkf-btn small" onClick={() => setShowRegistration( true )}>Jetzt Registrieren</button>
                    <button type="submit" className="fkf-btn">Login</button>
                </form>
            </div>
        </div>
    )
}