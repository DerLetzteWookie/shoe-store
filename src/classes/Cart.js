export default class Cart {
    constructor( params = {} ) {
        this.products = params.products !== undefined ? params.products : [];
        this.expireDate = params.expireDate !== undefined ? params.expireDate : this.generatExpireData();
    }

    add( productId = null, amount = null ) {
        if( productId === null || amount === null ) { return; }
        this.products.push({productId:productId,amount:amount});
    }

    generatExpireData() {
        var date = new Date();
        date.setDate(date.getDate() + 1);
        return date;
    }
}