export default class Order {
    constructor( params = {} ) {
        this.id = params.id !== undefined ? params.id : `ORDER_${Date.now()}`;
        this.date = params.date !== undefined ? new Date(params.date) : null;
        this.userId = params.userId !== undefined ? params.userId : "";
        this.shippingMethod = params.shippingMethod !== undefined ? params.shippingMethod : "";
        this.paymentMethod = params.paymentMethod !== undefined ? params.paymentMethod : "";
        this.products = params.products !== undefined ? params.products : [];
    }
}


