export default class User {
    constructor( params = {} ) {
        this.id = params.id !== undefined ? params.id : `USER_${Date.now()}`;
        this.name = params.name !== undefined ? params.name : "";
        this.mail = params.mail !== undefined ? params.mail : "";
        this.password = params.password !== undefined ? params.password : "";
        this.firstname = params.firstname !== undefined ? params.firstname : "";
        this.lastname = params.lastname !== undefined ? params.lastname : "";
        this.address = params.address !== undefined ? new Address(params.address) : new Address();
    }
}

class Address {
    constructor( params = {} ) {
        this.street = params.street !== undefined ? params.street : "";
        this.number = params.number !== undefined ? params.number : "";
        this.city = params.city !== undefined ? params.city : "";
        this.zipcode = params.zipcode !== undefined ? params.zipcode : "";
    }
}