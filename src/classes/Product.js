export default class Product {
    constructor( params = {} ) {
        this.articleNumber = params.articleNumber !== undefined ? params.articleNumber : "";
        this.name = params.name !== undefined ? params.name : "";
        this.description = params.description !== undefined ? params.description : "";
        this.price = params.price !== undefined ? params.price : "";
        this.image = params.image !== undefined ? params.image : "";
    }
}