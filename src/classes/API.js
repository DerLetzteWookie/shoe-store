import User from "./User";
import Product from "./Product";
import Order from "./Order";
import Cart from "./Cart";

const API = (function(module){
    const STORAGE_PRODUCTS = "FKF_PRODUCTS";
    const STORAGE_USERS = "FKF_USERS";
    const STORAGE_ORDERS = "FKF_ORDERS"
    const STORAGE_CART = "FKF_CART";

    function init() {
        initUserStorage();
        initProductStorage();
        initOrdersStorage();
        initCartStorage();
    }

    function initProductStorage() {
        if( localStorage.getItem(STORAGE_PRODUCTS) !== null ) { return; }
        console.log("initProductStorage");
        var products = [
            new Product({articleNumber: 100,name:"Skate Sneaker", description:"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",price:105.50,image:"shoe_001.jpg"}),
            new Product({articleNumber: 200,name:"Red Sneaker", description:"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",price:80.25,image:"shoe_002.jpg"}),
            new Product({articleNumber: 300,name:"Mustard Sneaker", description:"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",price:50.00,image:"shoe_003.jpg"}),
            new Product({articleNumber: 400,name:"Flying Sneaker", description:"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",price:200.20,image:"shoe_004.jpg"}),
            new Product({articleNumber: 500,name:"Green Sneaker", description:"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",price:45.56,image:"shoe_005.jpg"}),
            new Product({articleNumber: 600,name:"Blue High heels", description:"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",price:100.99,image:"shoe_006.jpg"}),
            new Product({articleNumber: 700,name:"Nice Shoes", description:"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",price:45.40,image:"shoe_007.jpg"})
        ];
        localStorage.setItem(STORAGE_PRODUCTS, JSON.stringify( products ) );
    }

    function initUserStorage() {
        if( localStorage.getItem(STORAGE_USERS) !== null ) { return; }
        console.log("initUserStorage");
        var testUser = new User({
            id: 1,
            name: "admin",
            mail: "admin@shop.de",
            password: "21232f297a57a5a743894a0e4a801fc3",
            firstname: "Max",
            lastname: "Mustermann",
            address:{
                street:"Musterstr.",
                number:"10A",
                city:"Berlin",
                zipcode:"12437"
            }
        });
        var users = [ testUser ];
        localStorage.setItem(STORAGE_USERS, JSON.stringify( users ) );
    }

    function initOrdersStorage() {
        if( localStorage.getItem(STORAGE_ORDERS) !== null ) { return; }
        console.log("initOrdersStorage");
        var orders = [
            new Order({userId:1,date:"2022-03-08T12:12:20.501Z",products:[{amount:3,articleNumber: 100,name:"Skate Sneaker", description:"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",price:105.50,image:"shoe_001.jpg"}]})
        ];
        localStorage.setItem(STORAGE_ORDERS, JSON.stringify( orders ) );
    }

    function initCartStorage() {
        if( localStorage.getItem(STORAGE_CART) !== null ) { return; }
        console.log("initCartStorage");
        var cart = new Cart();
        localStorage.setItem(STORAGE_CART, JSON.stringify( cart ) );
    }

    function createResponse(message = "", status = 500, data = null ) {
        return {
            status: status,
            data: data,
            message: message
        };
    }

    function login( mail = null, password = null ) {
        return new Promise((resolve, reject) => {
            if( mail === null || password === null ) {
                reject( createResponse( "Username or Password is not defined." ) );
            }
            var storageUsers = getStorageUsers();
            if( storageUsers === null ) {
                reject( createResponse( "Error in Database. Please contact Support." ) );
            }
            var foundUser = storageUsers.filter(user => user.mail === mail && user.password === password);
            if( !Array.isArray(foundUser) || foundUser.length <= 0 ) {
                reject( createResponse( "Mail or Password are wrong." ) );
            }

            resolve( createResponse( "Login success!", 200, foundUser[0] ) );
        });
    }

    function registration( mail = null, password = null ) {
        return new Promise((resolve, reject) => {
            var newUser = null;
            if( mail === null || password === null ) {
                reject( createResponse( "Username or Password is not defined." ) );
            }
            var storageUsers = getStorageUsers();
            if( storageUsers === null ) {
                reject( createResponse( "Error in Database. Please contact Support." ) );
            }
            var foundUser = storageUsers.filter(user => user.mail === mail );
            if( !Array.isArray(foundUser) || foundUser.length > 0 ) {
                reject( createResponse( "Mail still exists" ) );
            }
            newUser = new User({mail:mail,password:password});

            storageUsers.push( newUser );

            setStorageUsers(storageUsers);

            resolve( createResponse( "Registration success!", 200, newUser ) );
        });
    }

    function loginAsGuest( mail = null ) {
        return new Promise((resolve, reject) => {
            var guestUser = null;
            if( mail === null || mail === "" ) {
                reject( createResponse("Canot create guest user. Mail is empty.") );
            }

            guestUser = new User({mail:mail});

            resolve( createResponse( "Guest user was created", 200, guestUser ) );
            
        });
    }

    function setStorageUsers( users ) {
        localStorage.setItem(STORAGE_USERS, JSON.stringify(users));
    }

    function updateObject(obj/*, …*/) {
        for (var i=1; i<arguments.length; i++) {
            for (var prop in arguments[i]) {
                var val = arguments[i][prop];
                if (typeof val == "object") // this also applies to arrays or null!
                updateObject(obj[prop], val);
                else
                    obj[prop] = val;
            }
        }
        return obj;
    }

    function updateUser( updatedUser = null ){
        return new Promise((resolve, reject) => {
            var users = [];
            if( updatedUser === null ) {
                reject( createResponse("Canot update user.") );
            }
            
            users = getStorageUsers();
            
            if( users === null ) {
                reject( createResponse("Database error. Please contact support") );
            }

            users = users.map(user => {
                if( user.id === updatedUser.id ) {
                    user = updateObject(user, updatedUser);
                    updatedUser = user;
                }
                return user;
            });

            setStorageUsers( users );

            resolve( createResponse( "Guest user was updated", 200, updatedUser ) );
            
        });
    }

    function getStorageUsers() {
        var storageUsers = localStorage.getItem(STORAGE_USERS);
        if( storageUsers === null ) { return null; }
        storageUsers = JSON.parse( storageUsers );
        if( !Array.isArray( storageUsers ) ) {
            return null;
        }
        return storageUsers;
    }

    function getStorageProducts() {
        var storageProducts = localStorage.getItem(STORAGE_PRODUCTS);
        if( storageProducts === null ) {
            return null;
        }
        storageProducts = JSON.parse(storageProducts);
        if( storageProducts === null ) {
            return [];
        }
        return storageProducts;
    }

    function getProducts() {
        return new Promise((resolve, reject) => {
            var products = getStorageProducts();

            if( products === null ) {
                reject( createResponse( "Database Error. Contact support." ) );
            }

            resolve( createResponse( "Success. Products found.", 200, products ) );
        });
    }

    function getProduct( productId = null ) {
        return new Promise((resolve, reject) => {
            var products = getStorageProducts();
            var foundProduct = null;

            if( productId === null ) {
                reject( createResponse( "No productId was found." ) );
            }

            if( products === null ) {
                reject( createResponse( "Database Error. Contact support." ) );
            }
            foundProduct = products.filter( product => product.articleNumber === parseInt(productId) );
            
            if( !Array.isArray(foundProduct) || foundProduct.length <= 0 ) {
                reject( createResponse( "No product was found" ) );
            }

            resolve( createResponse( "Success. Product found.", 200, foundProduct[0] ) );
        });
    }

    function getStorageCart() {
        var storageCart = localStorage.getItem(STORAGE_CART);

        if( storageCart === null ) {
            return null;
        }
        storageCart = JSON.parse(storageCart);

        if( storageCart === null ) {
            return null;
        }

        return storageCart;
    }

    function getCart() {
        return new Promise((resolve, reject) => {
            var storageCart = getStorageCart();

            if( storageCart === null ) {
                reject( createResponse("Error in database. Please contact Support.") );
            }

            resolve( createResponse("Succes.", 200, storageCart) );
        });
    }

    function getStorageOrders() {
        var storageOrders = localStorage.getItem(STORAGE_ORDERS);

        if( storageOrders === null ) {
            return null;
        }
        storageOrders = JSON.parse(storageOrders);

        if( !Array.isArray(storageOrders) ) {
            return [];
        }
        return storageOrders;
    }

    function setStorageOrders( orders ) {
        localStorage.setItem(STORAGE_ORDERS, JSON.stringify(orders) );
    }

    function finishOrder( order = null ) {
        return new Promise((resolve, reject) => {
            var storageOrders = null;
            if( order === null ) {
                reject( createResponse("Error. Order is invalid.") );
            }
            order = new Order( order );

            if( !order.userId || !order.paymentMethod || !order.shippingMethod || order.products.length <= 0 ) {
                reject( createResponse("Error. Order is invalid.") );
            }
            order.date = new Date();
            storageOrders = getStorageOrders();
            storageOrders.push( order );
            setStorageOrders( storageOrders );
            clearStorageCart();
            resolve( createResponse("Succes.", 200) );
        });
    }

    function getUserOrders( userId = null ) {
        return new Promise((resolve, reject) => {
            var storageOrders = null;
            var userOrders = [];
            if( userId === null ) {
                reject( createResponse("Error. userId not found.") );
            }

            storageOrders = getStorageOrders();
            
            userOrders = storageOrders.filter(order => order.userId === userId );

            resolve( createResponse("Succes.", 200, userOrders) );
        });
    }

    function setStorageCart( cart ) {
        localStorage.setItem( STORAGE_CART, JSON.stringify(cart) );
    }

    function clearStorageCart( ) {
        localStorage.setItem( STORAGE_CART, JSON.stringify( new Cart() ) );
    }

    function addToCart(product, amount = 1) {
        return new Promise((resolve, rejected) => {
            var storageCart = getStorageCart();
            if( storageCart === null ) {
                storageCart = new Cart();
            }

            console.log( product );
            console.log( storageCart );
    
            if( !product.hasOwnProperty("amount") ) {
                product["amount"] = amount ? amount : 1;
            }
    
            if( storageCart.products.filter(cartProduct => parseInt(cartProduct.articleNumber) === parseInt(product.articleNumber) ).length > 0 ) {
                var newStorageCart = [];
                newStorageCart = storageCart.products.map( cartProduct => {
                    if( parseInt(cartProduct.articleNumber) === parseInt(product.articleNumber) ) {
                        cartProduct.amount += amount;
                    }
                    return cartProduct;
                } );
                console.log( newStorageCart );
                storageCart.products = newStorageCart;
            } else {
                console.log( "IS NOT IN CART...PUSH" );
                storageCart.products.push(product);
            }
            console.log( storageCart );
            setStorageCart( storageCart );
            resolve( createResponse( "Item was added to Cart.", 200 ) );
        });
        
    }

    function removeFromCart(product) {
        return new Promise((resolve, rejected) => {
            var storageCart = getStorageCart();
            
            if( storageCart === null ) {
                storageCart = new Cart();
            }
    
    
            if( storageCart.products ) {
                storageCart.products = storageCart.products.filter( cartProduct => parseInt(cartProduct.articleNumber) !== parseInt(product.articleNumber) );
            }
            setStorageCart( storageCart );
            resolve( createResponse( "Item was removed from Cart.", 200 ) );
        });
        
    }

    module.init = init;
    module.login = login;
    module.getProducts = getProducts;
    module.getProduct = getProduct;
    module.getCart = getCart;
    module.addToCart = addToCart;
    module.removeFromCart = removeFromCart;
    module.loginAsGuest = loginAsGuest;
    module.updateUser = updateUser;
    module.finishOrder = finishOrder;
    module.getUserOrders = getUserOrders;
    module.registration = registration;

    return module;
})({});

export default API;