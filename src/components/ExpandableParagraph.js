import React, { useState, useRef } from "react";

export default function ExpandableParagraph({className,children}) {
    const [isExpanded, setIsExpanded] = useState( false );
    var ref = useRef();

    return(
        <>
            <p className={`fkf-expandable-paragraph ${className} ${isExpanded ? "is--expanded" : ""} ${(ref.current && ref.current.offsetHeight < 100) ? "is--expanded" : ""}`} ref={ref}>{children}</p>
            {(ref.current && ref.current.offsetHeight) >= 100 ?
                <button className="fkf-expandable-paragraph__btn" onClick={() => setIsExpanded(!isExpanded)}>{isExpanded ? "weniger lesen" : "mehr lesen"}</button>
            : ""}
        </>
    )
}