import React, { useEffect, useState } from "react";
import { NavLink } from 'react-router-dom';
import API from "../classes/API";

import Loading from "./Loading";

import ProductListItem from "./ProductListItem";

export default function ProductList({children}) {
    const [ products, setProducts ] = useState( [] );
    const [ isLoading, setIsLoading ] = useState( true );

    useEffect(function() {
        if( isLoading ) {
            API.getProducts().then(response => {
                if( response.status === 200 ) {
                    setProducts( response.data, setIsLoading( false ) );
                }
            }).catch(error => {
                console.log(error.message);
                setIsLoading( false );
            });
        }
    }, [isLoading]);

    if( isLoading ) {
        return (<Loading />);
    }

    return(
        <div className="fkf-product-list">
            {children}
            <ul>
                {products.map((product, index) => <ProductListItem key={`PRODUCT_${index}`} product={product} ><NavLink className="fkf-btn" to={`/products/${product.articleNumber}`} title="Mehr erfahren">Mehr erfahren</NavLink></ProductListItem> )}
            </ul>
        </div>
    )
}