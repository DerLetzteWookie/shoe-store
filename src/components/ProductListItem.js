import React from "react";
import { NavLink } from "react-router-dom";

export default function ProductListItem({children,product}) {
    return(
        <li className="fkf-product-item">
            <div className="fkf-product-item__image">
                <img src={`${process.env.PUBLIC_URL}/assets/images/products/${product.image}`} alt="" title="" />
            </div>
            <div className="fkf-product-item__content">
                <p className="fkf-product-item__title">{product.name ? product.name : "-"}</p>
                <p className="fkf-product-item__price">{product.price ? product.price.toFixed(2) : "-"}€</p>
                {children}
            </div>
        </li>
    );
}