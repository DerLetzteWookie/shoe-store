import React, { useEffect, useState, useContext, useRef, useCallback } from "react";
import { AppStateContext } from '../components/AppStateProvider';

import API from "../classes/API";

import Loading from "./Loading";

import ProductListItem from "./ProductListItem";
import { NavLink } from "react-router-dom";

export default function Cart() {
    const [isLoading, setIsLoading] = useState( true );
    const [cart, setCart] = useState([]);
    const { CartIsOpen, SetCartIsOpen } = useContext(AppStateContext);
    const ref = useRef(null);
    var totalCartValue = 0;

    const onClickOutside = useCallback(() => {
        if( CartIsOpen ) {
            SetCartIsOpen( false );
        }
    }, [CartIsOpen, SetCartIsOpen]);

    const removeFromCartHandler = (product) => {
        API.removeFromCart(product).then(response => {
            if( response.status === 200 ) {
                setIsLoading( true );
            }
        })
    }

    useEffect(function(){
        if( isLoading ) {
            API.getCart().then(response => {
                if( response.status === 200 ) {
                    setCart( response.data, setIsLoading( false ) );
                }
            }).catch(error => {
                console.log(error);
                setIsLoading( false );
            })
        }
        const handleClickOutside = (event) => {
            if (ref.current && !ref.current.contains(event.target)) {
                onClickOutside && onClickOutside();
            }
        };
        document.addEventListener('click', handleClickOutside, true);
        return () => {
        document.removeEventListener('click', handleClickOutside, true);
        };
    }, [ isLoading, onClickOutside ]);

    if( isLoading ) {
        return <Loading />;
    }

    if( cart && cart.products ) {
        cart.products.forEach(product => {
            totalCartValue += parseInt(product.amount) * product.price;
        });
    }

    return(
        <aside className="fkf-cart" ref={ref}>
            <button className="fkf-cart__close-btn" onClick={() => SetCartIsOpen(false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
            </button>
            <p className="fkf-cart__title">Warenkrob</p>
            {cart && cart.products && cart.products.length <= 0 ? <p>Warenkorb ist leer</p> : 
                <>
                    <ul className="fkf-cart__items">{cart.products ? cart.products.map((product, index) => <ProductListItem key={`CART_PRODUCT_ITEM_${index}`} product={product}><p className="fkf-product-item__amount">{product.amount ? product.amount : 0}</p><button className="fkf-btn" onClick={() => removeFromCartHandler(product)}><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></button></ProductListItem>) : ""}</ul>
                    <p className="fkf-checkout__overview__total-price">Insgesamt: {totalCartValue}€</p>
                    <NavLink className="fkf-btn fkf-cart__checkout-btn" to="checkout">Zur Kasse</NavLink>
                </>
            }
        </aside>
    )
}