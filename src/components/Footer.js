import React from "react";

export default function Footer() {
    return(
        <footer className="fkf-footer">
            <ul></ul>
            <p>Made with 🔥 for best it AG ❤️</p>
        </footer>
    );
}