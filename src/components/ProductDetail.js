import React, { useEffect, useState, useContext } from "react";
import { NavLink, useParams } from "react-router-dom";

import API from "../classes/API";
import Product from "../classes/Product";

import { AppStateContext } from '../components/AppStateProvider';

import Loading from "./Loading";

export default function ProductDetail({match}) {
    const [product, setProduct] = useState( null );
    const [isLoading, setIsLoading] = useState( true );
    const { SetCartIsOpen } = useContext(AppStateContext);

    let routerParams = useParams();

    const addToCartHandler = function() {
        API.addToCart(product).then(response => {
            if( response.status === 200 ) {
                SetCartIsOpen( true );
            }
        })
    }

    useEffect(function(){
        if( isLoading ) {
            if( routerParams.productId ) {
                API.getProduct( routerParams.productId ).then(response => {
                    if( response.status === 200 ) {
                        setProduct( new Product( response.data ), setIsLoading( false ) );
                    }
                }).catch(error => {
                    console.log(error.message);
                    setIsLoading( false )
                })
            } else {
                setIsLoading( false );
            }
        }
    }, [isLoading, routerParams]);

    if( isLoading ) {
        return <Loading />;
    }

    if( product === null ) {
        return (
            <div>
                <p>No product was found</p>
            </div>
        )
    }

    return(
        <div className="fkf-product-detail">
            <NavLink to="../" className="fkf-product-detail__back-btn" title="Back">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>
            </NavLink>
            <div className="fkf-product-detail__image">
                <img src={`${process.env.PUBLIC_URL}/assets/images/products/${product.image}`} alt="" title="" />
            </div>
            <div className="fkf-product-detail__content">
                <p className="fkf-product-detail__title">{product.name ? product.name : "-"}</p>
                <p className="fkf-product-detail__description">
                    {product.description ? product.description : "-"}
                </p>
                <p className="fkf-product-detail__price">Preis: {product.price ? product.price.toFixed(2) : "-"}€</p>
                <button className="fkf-btn" onClick={addToCartHandler}>
                    in den Warenkorb <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-shopping-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>
                </button>
            </div>
        </div>
    )
}