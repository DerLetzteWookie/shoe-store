import React, { useContext } from "react";
import {
  BrowserRouter,
  Routes,
  Route,
  NavLink
} from "react-router-dom";

import { AppStateContext } from './components/AppStateProvider';
import API from "./classes/API";

import Navigation from "./components/Navigation";
import Cart from "./components/Cart";
import Footer from "./components/Footer";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Products from "./pages/Products";
import Checkout from "./pages/Checkout";
import Profile from "./pages/Profile";

import Snackbar from './components/Snackbar';

import Logo from './assets/images/logo.png';

function App() {
  const { Snackbars, CloseSnackbar, CartIsOpen } = useContext(AppStateContext);
  API.init();
  return (
    <>
      {Snackbars.map(snackbar => <Snackbar snackbar={snackbar} closeSnackbar={CloseSnackbar} />)}
      <BrowserRouter>
        <NavLink to="/" className="fkf-main-logo">
          <img src={Logo} alt="Shoe Store" title="Shoe Store" />
        </NavLink>
        <Navigation />
        {CartIsOpen ? <Cart /> : ""}
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="products/*" element={<Products />} />
          <Route path="checkout/*" element={<Checkout />} />
          <Route path="/profile/*" element={<Profile />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </BrowserRouter>
      <Footer />
    </>
  );
}

export default App;
